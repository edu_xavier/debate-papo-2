﻿/// <reference path="lib/tokenize.js" />
/*
    Responsável em organizar as mensagens de forma estruturada.
*/
var Improver = {    
    messages:[], //indica as mensagens estruturadas
    /// adiciona a mensagem na lista
    attach: function (unstructured_message, user)
    {
        var structured = ToJson(unstructured_message);
        this.messages.push(tokenize.build(structured));
        
        function ToJson(message)
        {
            return {
                id: message.attr("id"),
                body: $(message).children('body').text(),
                rel: message.attr("rel"),
                author: user
            };
        }        
    }   
};