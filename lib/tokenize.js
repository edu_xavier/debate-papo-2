String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };

var tokenize =
{
    /**
    * Respons�vel em construir a estrutura da mensagem
    *
    * @words {String} mensagem crua, sem tratamento algum
    * @return {Array} Return a mensagem com a estrutura de tokeniza��o
    */
    build: function (message)
    {
        message.bag = [];
        var of_words    = message.texto.split(' ');
        message.bag = this.produce_bag(of_words);

        return message;
    },
    /**
    * Respons�vel em produzir ou atualizar a bolsa de palavras
    *
    * @words {Array} conjunto de palavras de uma mensagem
    * @return {Array} Return conjunto de palavras vetorizadas
    */
    produce_bag: function(words)
    {
        var bag = [];
        for (var j = 0; j < words.length; j++)
        {
            var word = words[j].toLowerCase()
                        .replace(/[(\!|\.|\,|\:|\%)|(0-9)]*/g, '');

            word = word.replace(/[(\�|\�|\�)]/g, 'a')
                        .replace(/(\�|\�|\�)/, 'e')
                        .replace('�', 'i')
                        .replace(/(\�|\�|\�)/, 'o')
                        .replace('�', 'u')
                        .replace('�', 'c')
                        .trim();

            if (word == "" || word.length <= 2)
                continue;

            //aplica stemming na palavra, j� em portuga!
            word = new Stem('portuguese')(word)

            //joga a plavra na bolsa ou incrementa sua frequ�ncia
            if (!this.any(word, bag))
                bag.push({ key: word, frequency: 1 });
        };

        return bag;
    },
    /**
    * M�todo que procura palvras e analisa a frequ�ncia de uso da mesma
    *
    * @word {String} palavra que ser� analisada
    * @bag {Array} total de palavras j� analisadas
    * @return {Boolean} Return true se existir a palavra
    */
    any: function(word, bag)
    {
        if (bag.length == 0)
            return false;

        for (var k = 0; k < bag.length; k++)
        {
            if (bag[k].key == word)
            {                
                bag[k].frequency++;
                return true;
            }
        }
        return false;
    }   
}